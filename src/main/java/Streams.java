import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Streams {
    public static void main(String[] args) {
        List<Person> people = Arrays.asList(new Person("Mateusz", "Kalinowski", 21, "Warszawa"),
                new Person("Inna", "Osoba", 22, "Kraków"),
                new Person("Jesczcze", "Inna", 23, "Warszawa"),
                new Person("Patrycja", "Malinowska", 21, "Warszawa"));

        List<Person> anotherPeople = Arrays.asList(new Person("Mateusz", "Kalinowski", 21, "Warszawa"),
                new Person("Ktos", "Tam", 22, "Poznań"));

    }

    public static Person getOldest(List<Person> data) {
        return data.stream().max(Comparator.comparingInt(Person::getAge)).get();
    }

    public static List<String> getCities(List<Person> data) {
        return data.stream().map(p -> p.getCity()).distinct().collect(Collectors.toList());
    }

    public static List<Person> getAllFemales(List<Person> people) {
        return people.stream().filter(n -> n.getName().endsWith("a")).collect(Collectors.toList());
    }

    public static Person getOldestFemale(List<Person> people) {
        return people.stream().filter(n -> n.getName().endsWith("a")).max(Comparator.comparingInt(Person::getAge))
                .orElseThrow(() -> new IllegalStateException("Nie mozna znalezc najstarszej kobiety, poniewaz brakuje kobiet."));
    }

    public static double getAvgAge(List<Person> people) {
        return people.stream().collect(Collectors.averagingDouble(Person::getAge));
    }

    public static double getAvgManAge(List<Person> people) {
        return people.stream().filter(m -> !m.getName().endsWith("a")).collect(Collectors.averagingDouble(Person::getAge));
    }

    public static List<Person> getCommonPeople(List<Person> list1, List<Person> list2){
        return list1.stream().filter(list2::contains).collect(Collectors.toList());
    }

    public static String getMostPopularCity(List<Person> people) {
        Map<String, Long> groupByCity = people.stream().collect(Collectors.groupingBy(Person::getCity, Collectors.counting()));
        return groupByCity.entrySet().stream().max(Comparator.comparingLong(Map.Entry::getValue)).get().getKey();
    }

}